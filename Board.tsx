import React, { Component } from 'react'
import { StyleSheet, View, FlatList, SafeAreaView, Button, Text } from 'react-native'
import Light from './Light'

type BoardStates = {
    size: number,
    lits: Array<boolean>,
    steps: number,
    stopped: boolean,
    win: boolean,
    timerSec: number,
    timerMin: number
    width: number
}

class Board extends Component<{}, BoardStates> {
    constructor(props: {}) {
        super(props)

        this.state = {
            size: 5,
            width: 350,
            lits: [],
            steps: 0,
            stopped: true,
            win: false,
            timerSec: 0,
            timerMin: 0
        }
    }


    componentDidMount = () => {
        const brd = []
        for (let i = 0; i < this.state.size * this.state.size; i++) {
            brd.push(false)
        }

        this.setState({
            lits: brd
        })

        this.shuffle()
    }

    tick = () => {
        setTimeout(() => {
            if (!this.state.stopped) {
                this.setState((prevState) => {
                    let sec: number
                    let min: number
                    if (prevState.timerSec + 1 === 60) {
                        sec = 0
                        min = prevState.timerMin + 1
                    } else {
                        sec = prevState.timerSec + 1
                        min = prevState.timerMin
                    }
                    return {
                        timerSec: sec,
                        timerMin: min
                    }
                })
                this.tick()
            }
        }, 1000)
    }

    shuffle = () => {
        for (let i = 0; i < 50; i++) {
            const rndm = Math.floor(Math.random() * 25)
            this.onClickHandler(rndm)
        }

        this.setState({
            steps: 0,
            stopped: false,
            timerSec: 0,
            timerMin: 0
        })

        this.tick()

        return
    }

    onClickHandler = (i: number) => {
        let isWin: boolean | undefined

        this.setState((prevState) => {
            const lit = prevState.lits.map((lt: boolean, index: number) => {
                if (
                    (index === i - prevState.size) ||
                    (index === i - 1 && (i % prevState.size) !== 0) ||
                    (index === i) ||
                    (index === i + 1 && (i % prevState.size) !== (prevState.size - 1)) ||
                    (index === i + prevState.size)
                ) {
                    return !lt
                } else {
                    return lt
                }
            })

            isWin = lit.find((lt: boolean) => lt)

            return {
                lits: lit,
                steps: (prevState.stopped) ? prevState.steps : prevState.steps + 1,
                stopped: isWin ? false : true,
                win: isWin ? false : true
            }
        })

        return
    }

    render() {
        let gridCols = ""
        let gridSize = this.state.width / this.state.size
        for (let i = 0; i < this.state.size; i++) {
            if (i > 0) {
                gridCols += " "
            }

            gridCols += `${gridSize}px`
        }

        return (
            <SafeAreaView>
                <View style={styles.button}>
                    <Button onPress={this.shuffle} title="New Game" />
                </View>
                <FlatList
                    data={this.state.lits}
                    numColumns={5}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) =>
                        <Light
                            lit={item}
                            buttonSize={this.state.width / this.state.size}
                            onClickHandler={() => (this.state.stopped) ? () => { } : this.onClickHandler(index)}
                        />
                    }
                />
                <View>{this.state.win ? <Text style={styles.win}>You won!</Text> : null}</View>
                <View style={styles.counters}>
                    <Text style={styles.timer}>Time: {(this.state.timerMin < 10) ? "0" : ""}{this.state.timerMin}:{(this.state.timerSec < 10) ? "0" : ""}{this.state.timerSec}</Text>
                    <Text style={styles.steps}> | Steps: {this.state.steps}</Text>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        marginTop: 10,
        marginBottom: 20,
    },
    counters: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    timer: {
        color: '#fff',
        fontSize: 25,
        textAlign: 'center',
    },
    steps: {
        color: '#fff',
        fontSize: 25,
        textAlign: 'center',
    },
    win: {
        color: '#ffffff',
        textTransform: 'uppercase',
        paddingTop: 58,
        paddingBottom: 10,
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: 75,
        marginBottom: 10,
        fontSize: 25,
        height: 150,
        width: 150,
        textAlign: 'center',
        backgroundColor: '#0aa000'
    }
})

export default Board