import React, { Component } from 'react'
import { View, StyleSheet, Pressable, GestureResponderEvent } from 'react-native'

type LightProps = {
    // key: number,
    lit: boolean,
    buttonSize: number,
    onClickHandler: (event: GestureResponderEvent) => void
}

class Light extends Component<LightProps, {}> {
    render() {
        return (
             <View>
                <Pressable style={[styles.btn, this.props.lit ? styles.light : styles.dark]} onPress={this.props.onClickHandler}></Pressable>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    btn: {
        borderLeftWidth: 3,
        borderRightWidth: 3,
        borderTopWidth: 3,
        borderBottomWidth: 3,
        width: 70,
        height: 70,
        margin: 2,
        borderRadius: 4,
    },
    light: {
        backgroundColor: '#ff0000',
        borderLeftColor: '#330000',
        borderBottomColor: '#330000',
        borderRightColor: '#ff6666',
        borderTopColor: '#ff6666',
    },
    dark: {
        backgroundColor: '#2f2f2f',
        borderLeftColor: '#000000',
        borderBottomColor: '#000000',
        borderRightColor: '#666666',
        borderTopColor: '#666666',
    }
})

export default Light